// SensorAgent implementation
package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"strings"
	"sync"
	"time"

	"bitbucket.org/mfkenney/go-power"
	"bitbucket.org/mfkenney/go-redisq"
	"bitbucket.org/mfkenney/go-sensor"
	"bitbucket.org/mfkenney/go-sensor/aanderaa"
	"bitbucket.org/mfkenney/go-sensor/fsi"
	"bitbucket.org/mfkenney/go-sensor/seabird"
	"bitbucket.org/mfkenney/go-sensor/wetlabs"
	"bitbucket.org/uwaploe/go-dputil"
	"github.com/garyburd/redigo/redis"
)

type SensorAgent struct {
	sens      sensor.SerialSensor
	sw        power.PowerSwitch
	warmup    time.Duration
	buflen    int
	startcmds []string
	endcmds   []string
}

func NewSensorAgent(cfg *SensorCfg, port io.ReadWriter) (*SensorAgent, error) {
	var (
		agent *SensorAgent
		err   error
	)

	switch strings.ToLower(cfg.Class) {
	case "sbe52mp":
		log.Println("Using Seabird SBE-52mp")
		agent = &SensorAgent{
			sens:   seabird.NewSbe52mp(cfg.Name, port, false),
			warmup: time.Duration(cfg.Warmup) * time.Millisecond,
			startcmds: []string{
				"",
				"outputctdo=y",
				"outputctdoraw=n",
				"outputsn=n",
				"overwritemem=y",
				"startprofile"},
			endcmds: []string{"stopprofile"}}
	case "optode":
		log.Println("Using Aanderaa Optode")
		agent = &SensorAgent{
			sens:   aanderaa.NewOptode(cfg.Name, port),
			warmup: time.Duration(cfg.Warmup) * time.Millisecond}
	case "acm":
		log.Println("Using FSI ACM")
		agent = &SensorAgent{
			sens:   fsi.NewAcm(cfg.Name, port),
			warmup: time.Duration(cfg.Warmup) * time.Millisecond}
	case "flntu":
		log.Println("Using Wetlabs FLNTU")
		agent = &SensorAgent{
			sens:   wetlabs.NewFlntu(cfg.Name, port),
			warmup: time.Duration(cfg.Warmup) * time.Millisecond}
	case "flcd":
		log.Println("Using Wetlabs FLCDR")
		agent = &SensorAgent{
			sens:   wetlabs.NewFlcd(cfg.Name, port),
			warmup: time.Duration(cfg.Warmup) * time.Millisecond}
	default:
		return nil, fmt.Errorf("Unknown sensor class: %v", cfg.Class)
	}

	agent.sw, err = power.NewTsmemPowerSwitch(cfg.Port.Switch)
	if err != nil {
		return nil, fmt.Errorf("Cannot access power switch for %v", cfg.Class)
	}

	// Set a data channel buffer size
	if cfg.Buflen > 0 {
		agent.buflen = cfg.Buflen
	} else {
		agent.buflen = 4
	}

	if cfg.Timeout > 0 {
		agent.sens.SetTimeout(time.Duration(cfg.Timeout) * time.Millisecond)
	}

	// Ensure the device is powered off
	agent.sw.Off()
	return agent, nil
}

func (agent *SensorAgent) setup() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	for _, cmd := range agent.startcmds {
		log.Printf("%s: < %q", agent.sens.Name(), cmd)
		err := agent.sens.Send([]byte(cmd))
		if err == nil {
			resp, err := agent.sens.Recv(ctx)
			if err == nil {
				log.Printf("%s: > %q", agent.sens.Name(), string(resp))
				time.Sleep(time.Millisecond * 100)
			} else {
				return err
			}

		} else {
			log.Printf("%s: error sending %q: %v",
				agent.sens.Name(), cmd, err)
			return err
		}
	}
	return nil
}

func (agent *SensorAgent) teardown() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	for _, cmd := range agent.endcmds {
		log.Printf("%s: < %q", agent.sens.Name(), cmd)
		err := agent.sens.Send([]byte(cmd))
		if err == nil {
			resp, err := agent.sens.Recv(ctx)
			if err == nil {
				log.Printf("%s: > %q", agent.sens.Name(), string(resp))
				time.Sleep(time.Millisecond * 100)
			}
		} else {
			log.Printf("%s: error sending %q: %v",
				agent.sens.Name(), cmd, err)
		}
	}
}

// Continuously Sample a sensor and send the data records to the Archiver
// queue.
func (agent *SensorAgent) sample(ctx context.Context, conn redis.Conn, qname string,
	wg *sync.WaitGroup) {
	defer wg.Done()

	dataq, err := redisq.NewQueue(conn, qname, 200)
	defer conn.Close()

	agent.sw.On()
	time.Sleep(agent.warmup)
	defer agent.sw.Off()

	agent.sens.SetTimeout(time.Second * 8)
	err = agent.setup()
	if err != nil {
		log.Printf("%s: setup error: %v", agent.sens.Name(), err)
		return
	}
	defer agent.teardown()
	agent.sens.SetTimeout(time.Second * 3)

	log.Printf("Begin sampling %q", agent.sens.Name())

	c := sensor.Sampler(ctx, agent.sens, 0, agent.buflen)

loop:
	for {
		select {
		case rec, more := <-c:
			if !more {
				break loop
			}
			dr := &dputil.DataRecord{
				Source: rec.Name,
				T:      rec.T}
			dr.Data, err = agent.sens.ParseData(rec.Data)
			if err == nil {
				v := dr.Serialize()
				err = dataq.Put(v...)
				if err != nil {
					log.Printf("Cannot queue data record: %v\n", err)
				}
			} else {
				log.Printf("%s: error parsing data: %v", rec.Name,
					err)
			}
		case <-ctx.Done():
			break loop
		}
	}

	log.Printf("End sampling %q", agent.sens.Name())
}
