// This program is part of the Deep Profiler project and manages the
// serial sensors on the DPC.
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-dputil"
	"github.com/garyburd/redigo/redis"
	"github.com/tarm/serial"
	"gopkg.in/yaml.v2"
)

// Default configuration
var defcfg = `
sensors:
  - name: ctd_1
    class: sbe52mp
    warmup: 1000
    buflen: 4
    port:
      device: /dev/ttyS0
      baud: 9600
      switch: "8160_LCD_D0"
  - name: optode_1
    class: optode
    warmup: 500
    buflen: 20
    port:
      device: /dev/ttyS3
      baud: 9600
      switch: "8160_LCD_D3"
  - name: flntu_1
    class: flntu
    warmup: 500
    buflen: 8
    port:
      device: /dev/ttyS5
      baud: 19200
      switch: "8160_LCD_D6"
  - name: flcd_1
    class: flcd
    warmup: 500
    buflen: 8
    port:
      device: /dev/ttyS4
      baud: 19200
      switch: "8160_LCD_D6"
  - name: acm_1
    class: acm
    warmup: 500
    buflen: 4
    port:
      device: /dev/ttyS1
      baud: 19200
      switch: "8160_LCD_D1"
`

var Version = "dev"
var BuildDate = "unknown"

type PortCfg struct {
	Device string
	Baud   int
	Switch string
}

type SensorCfg struct {
	Name    string
	Class   string
	Warmup  int
	Buflen  int
	Port    PortCfg
	Timeout int
}

type ProgCfg struct {
	Sensors []SensorCfg
}

func new_pool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Manage the DPC sensor sampling\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	cfgfile := flag.String("cfgfile", "",
		"Path to the configuration file")
	dumpcfg := flag.Bool("dumpcfg", false, "Dump default config to stdout")
	dqname := flag.String("queue", "archive:queue",
		"Name of Redis list for the data queue")
	cname := flag.String("channel", "events.mmp",
		"Name of Redis channel for profile event messages")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Fprintf(os.Stdout, "---\n%s\n", defcfg)
		os.Exit(0)
	}

	var contents []byte
	var err error

	if *cfgfile != "" {
		contents, err = ioutil.ReadFile(*cfgfile)
		if err != nil {
			panic(err)
		}
	} else {
		contents = []byte(defcfg)
	}

	cfg := ProgCfg{}
	err = yaml.Unmarshal(contents, &cfg)
	if err != nil {
		log.Fatal("Configuration error: %v", err)
	}

	// Pool of Redis connections
	pool := new_pool("localhost:6379")

	// Initialize all SensorAgent's
	agents := make([]*SensorAgent, 0)
	masks := make(map[string]uint)
	for index, scfg := range cfg.Sensors {
		port, err := serial.OpenPort(&serial.Config{
			Name:        scfg.Port.Device,
			Baud:        scfg.Port.Baud,
			ReadTimeout: time.Second * 5})
		if err == nil {
			a, err := NewSensorAgent(&scfg, port)
			if err == nil {
				agents = append(agents, a)
				masks[a.sens.Name()] = 1 << uint(index)
			} else {
				log.Println(err)
			}
		} else {
			log.Println(err)
		}
	}

	if len(agents) == 0 {
		log.Fatal("No sensors found!")
	}

	// Pubsub connection
	psc := redis.PubSubConn{Conn: pool.Get()}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			psc.Unsubscribe()
		}
	}()

	psc.Subscribe(*cname)
	log.Println("Waiting for messages")

	var (
		exclude   []string
		sens_mask uint
		cancel    context.CancelFunc
		ctx       context.Context
		wg        sync.WaitGroup
	)
	conn := pool.Get()

evloop:
	for {
		switch msg := psc.Receive().(type) {
		case error:
			log.Println(msg)
			break evloop
		case redis.Subscription:
			if msg.Count == 0 {
				log.Println("Pubsub channel closed")
				break evloop
			}
		case redis.Message:
			ev := dputil.ParseEvent(string(msg.Data))
			switch ev.Name {
			case "profile:start":
				if cancel != nil {
					cancel()
					log.Println("Waiting for tasks to exit")
					wg.Wait()
					cancel = nil
					log.Println("Power-on delay")
					time.Sleep(time.Second * 3)
				}
				ctx, cancel = context.WithCancel(context.Background())

				// Get list of sensors to exclude
				exclude, _ = redis.Strings(conn.Do("LRANGE",
					"sens:exclude", 0, -1))
				log.Printf("Excluding sensors: %q", exclude)
				sens_mask = ^uint(0)
				for _, name := range exclude {
					if mask, ok := masks[name]; ok {
						sens_mask = sens_mask &^ mask
					} else {
						log.Printf("Unknown sensor: %q", name)
					}
				}
				log.Printf("Sensor mask: %b", sens_mask)

				for i, agent := range agents {
					if ((1 << uint(i)) & sens_mask) != 0 {
						wg.Add(1)
						go agent.sample(ctx, pool.Get(), *dqname, &wg)
					}
				}
			case "profile:end":
				if cancel != nil {
					cancel()
					log.Println("Waiting for tasks to exit")
					wg.Wait()
					cancel = nil
				}
			}

		}
	}

	if cancel != nil {
		cancel()
		log.Println("Waiting for tasks to exit")
		wg.Wait()
	}
}
